import React from 'react';
import {Modal} from 'react-bootstrap';
import './style.scss';

export default (OriginalComponent) => class PopupWrapper extends React.Component {

  render() {
    const {isOpen, changeState} = this.props;

    return (
      <div className="modal-container" style={{height: 200}}>
        <Modal
          show={isOpen}
          onHide={changeState}
          container={this}
          aria-labelledby="contained-modal-title">

          <OriginalComponent {...this.props}/>
        </Modal>
      </div>
    );
  }

}
