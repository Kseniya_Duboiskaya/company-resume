import {
  GET_TECNOLOGY_BY_ID,
  GET_ALL_TECHNOLOGIES,
  UPDATE_TECHNOLOGY,
  DELETE_TECHNOLOGY_BY_ID,
  EDIT_TECHNOLOGY_BY_ID,
  FILTER_TECHNOLOGIES,
  ADD_TECHNOLOGY
} from '../const/action-creator-technologies';
import {START, SUCCESS} from '../const/common';
import {OrderedMap, Map, Record} from 'immutable';
import {arrToMap} from '../helpers';

const TechnolgyModel = Record({
  description: undefined,
  expertises:  [],
  id:          undefined,
  name:        undefined,
  tags:        []
});

const ReducerState = Record({
  loading:  false,
  loaded:   false,
  entities: new OrderedMap({})
});

const defaultState = new ReducerState();

export default (technologiesState = defaultState, action) => {
  const {type, response, payload} = action;

  switch (type) {
    case GET_TECNOLOGY_BY_ID + START:
      return technologiesState.set('loading', true);

    case GET_TECNOLOGY_BY_ID + SUCCESS:
      return technologiesState.setIn(['entities', payload.technologyId], new TechnolgyModel(response))
                              .set('loading', false)
                              .set('loaded', true);

    case UPDATE_TECHNOLOGY + SUCCESS:
      return technologiesState.setIn(['entities', payload.id], new TechnolgyModel(response));

    case GET_ALL_TECHNOLOGIES + SUCCESS:
    case FILTER_TECHNOLOGIES + SUCCESS:
      return technologiesState.set('entities', arrToMap(response, TechnolgyModel));

    case DELETE_TECHNOLOGY_BY_ID + SUCCESS:
      return technologiesState.deleteIn(['entities', payload.technologyId]);

    case EDIT_TECHNOLOGY_BY_ID + SUCCESS:
      return technologiesState.setIn(['entities', payload.technologyId], new TechnolgyModel(response));

    case ADD_TECHNOLOGY + SUCCESS:
      return technologiesState.mergeIn(['entities'], {[response.id]: new TechnolgyModel(response)});
  }

  return technologiesState;
}