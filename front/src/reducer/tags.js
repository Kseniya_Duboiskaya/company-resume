import {
  GET_TAGS_BY_EXPERTISE,
  GET_ALL_TAGS,
  ADD_TAG,
  DELETE_TAG_BY_ID,
  EDIT_TAG_BY_ID
} from '../const/action-creator-tags';
import {START, SUCCESS} from '../const/common';
import {OrderedMap, Map, Record} from 'immutable';
import {arrToMapWithSingleNesting, test} from '../helpers';

const TagModel = Record({
  tag:          {
    id:   undefined,
    name: undefined
  },
  technologies: []
});

const ReducerState = Record({
  loading:  false,
  loaded:   false,
  entities: new OrderedMap({})
});

const defaultState = new ReducerState();

export default (tagsState = defaultState, action) => {
  const {type, response, payload} = action;

  switch (type) {
    case GET_TAGS_BY_EXPERTISE + SUCCESS:
      return tagsState.set('entities', arrToMapWithSingleNesting(response, TagModel, 'tag'));

    case GET_ALL_TAGS + SUCCESS:
      const mapWithAllTags = response.reduce((acc, item) => {
        return acc.set(item.id, new TagModel({
          tag: item
        }))
      }, OrderedMap({}));

      return tagsState.set('entities', mapWithAllTags);

    case ADD_TAG + SUCCESS:
      return tagsState.mergeIn(['entities'], {[response.id]: new TagModel({tag: response})});

    case DELETE_TAG_BY_ID + SUCCESS:
      return tagsState.deleteIn(['entities', payload.tagId]);

    case EDIT_TAG_BY_ID + SUCCESS:
      return tagsState.setIn(['entities', payload.tagId], new TagModel({tag: response}));

  }

  return tagsState;
}