import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux';
import expertises from './expertises';
import tags from './tags';
import technologies from './technologies';

export default combineReducers({
  router: routerReducer,
  expertises,
  tags,
  technologies,
})