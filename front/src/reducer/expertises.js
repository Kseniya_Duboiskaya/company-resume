import {
  GET_ALL_EXPERTISES,
  GET_EXPERTISE_BY_ID,
  ADD_TECHNOLOGY_TO_EXPERTISE,
  DELETE_TECHNOLOGY_FROM_EXPERTISE,
  DELETE_EXPERTISE_BY_ID,
  ADD_EXPERTISE,
  EDIT_EXPERTISE_BY_ID
} from '../const/action-creator-dashboard';
import {START, SUCCESS, FAIL} from '../const/common';
import {OrderedMap, Map, Record} from 'immutable';
import {arrToMap} from '../helpers';

const ExpertiseModel = Record({
  description:  undefined,
  name:         undefined,
  id:           undefined,
  technologies: []
});

const ReducerState = Record({
  loading:  false,
  loaded:   false,
  entities: new OrderedMap({})
});

const defaultState = new ReducerState();

export default (expertiseState = defaultState, action) => {
  const {type, response, payload} = action;

  switch (type) {
    case GET_ALL_EXPERTISES + SUCCESS:
      return expertiseState.set('entities', arrToMap(response, ExpertiseModel));

    case GET_EXPERTISE_BY_ID + SUCCESS:
      return expertiseState.setIn(['entities', payload.expertiseId], new ExpertiseModel(response));

    case ADD_TECHNOLOGY_TO_EXPERTISE + SUCCESS:
      const oldTechnologies = expertiseState.getIn(['entities', payload.expertiseId, 'technologies']);
      const newTechnologies = oldTechnologies.concat(response);
      return expertiseState.setIn(['entities', payload.expertiseId, 'technologies'], newTechnologies);

    case DELETE_TECHNOLOGY_FROM_EXPERTISE + SUCCESS:
      const technologiesWithoutDeleted = expertiseState.getIn(['entities', payload.expertiseId, 'technologies'])
                                                       .filter((technology) => {
                                                         return technology.id !== response.id;
                                                       });
      return expertiseState.setIn(['entities', payload.expertiseId, 'technologies'], technologiesWithoutDeleted);

    case DELETE_EXPERTISE_BY_ID + SUCCESS:
      return expertiseState.deleteIn(['entities', payload.expertiseId]);

    case ADD_EXPERTISE + SUCCESS:
      return expertiseState.mergeIn(['entities'], {[response.id]: new ExpertiseModel(response)});

    case EDIT_EXPERTISE_BY_ID + SUCCESS:
      return expertiseState.setIn(['entities', payload.expertiseId], new ExpertiseModel(response));
  }

  return expertiseState;
}