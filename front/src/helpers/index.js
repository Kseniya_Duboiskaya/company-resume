import {OrderedMap, Map} from 'immutable';

export function arrToMap(arr, Model = Map) {

  return arr.reduce((acc, item) =>
    acc.set(item.id, new Model(item)), OrderedMap({}));
}

export function mapToArr(obj) {
  return obj.valueSeq()
            .toArray();
}

export function arrToMapWithSingleNesting(arr, Model = Map, propertyName) {
  return arr.reduce((acc, item) =>
    acc.set(item[propertyName].id, new Model(item)), OrderedMap({}));
}