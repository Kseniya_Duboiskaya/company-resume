import {
  GET_ALL_TAGS,
  GET_TAGS_BY_EXPERTISE,
  ADD_TAG,
  DELETE_TAG_BY_ID,
  EDIT_TAG_BY_ID
} from '../const/action-creator-tags';
import {
  GET_ALL_EXPERTISES,
  GET_EXPERTISE_BY_ID,
  ADD_TECHNOLOGY_TO_EXPERTISE,
  DELETE_TECHNOLOGY_FROM_EXPERTISE,
  DELETE_EXPERTISE_BY_ID,
  ADD_EXPERTISE,
  EDIT_EXPERTISE_BY_ID
} from '../const/action-creator-dashboard';
import {
  GET_TECNOLOGY_BY_ID,
  UPDATE_TECHNOLOGY,
  GET_ALL_TECHNOLOGIES,
  DELETE_TECHNOLOGY_BY_ID,
  EDIT_TECHNOLOGY_BY_ID,
  FILTER_TECHNOLOGIES,
  ADD_TECHNOLOGY
} from '../const/action-creator-technologies';

export function getAllTags() {
  return {
    type:    GET_ALL_TAGS,
    callAPI: {
      method: 'GET',
      api:    `tags`
    }
  }
}

export function getExpertises() {
  return {
    type:    GET_ALL_EXPERTISES,
    callAPI: {
      method: 'GET',
      api:    'expertises'
    }
  }
}

export function getAllTechnologies() {
  return {
    type:    GET_ALL_TECHNOLOGIES,
    callAPI: {
      method: 'GET',
      api:    'technologies'
    }
  }
}

export function getTagsByExpertise(expertiseId) {
  return {
    type:    GET_TAGS_BY_EXPERTISE,
    payload: {expertiseId},
    callAPI: {
      method: 'POST',
      api:    'tags',
      body:   {expertiseId},
    }
  }
}

export function getTechnologyById(technologyId) {
  return {
    type:    GET_TECNOLOGY_BY_ID,
    payload: {technologyId},
    callAPI: {
      method: 'GET',
      api:    `technologies/${technologyId}`,
    }
  }
}

export function getExpertiseById(expertiseId) {
  return {
    type:    GET_EXPERTISE_BY_ID,
    payload: {expertiseId},
    callAPI: {
      method: 'GET',
      api:    `expertises/${expertiseId}`
    }
  }
}

export function updateTechnology(entity, id) {
  return {
    type:    UPDATE_TECHNOLOGY,
    payload: {id},
    callAPI: {
      method: 'PUT',
      api:    `technologies/${id}`,
      body:   entity,
    }
  }
}

export function deleteTechnologyFromExpertise(expertiseId, technologyId) {
  return {
    type:    DELETE_TECHNOLOGY_FROM_EXPERTISE,
    payload: {expertiseId},
    callAPI: {
      method: 'DELETE',
      api:    `technologies/${technologyId}/expertises/${expertiseId}`,
    }
  }
}

export function addTechnologyToExpertise(expertiseId, technologyId) {
  return {
    type:    ADD_TECHNOLOGY_TO_EXPERTISE,
    payload: {expertiseId},
    callAPI: {
      method: 'PUT',
      api:    `technologies/${technologyId}/expertises/${expertiseId}`,
    }
  }
}

export function deleteTechnology(technologyId) {
  return {
    type:    DELETE_TECHNOLOGY_BY_ID,
    payload: {technologyId},
    callAPI: {
      method: 'DELETE',
      api:    `technologies/${technologyId}`
    }
  }
}

export function deleteExpertise(expertiseId) {
  return {
    type:    DELETE_EXPERTISE_BY_ID,
    payload: {expertiseId},
    callAPI: {
      method: 'DELETE',
      api:    `expertises/${expertiseId}`
    }
  }
}

export function editExpertise(expertiseId, changeExpertise) {
  return {
    type:    EDIT_EXPERTISE_BY_ID,
    payload: {expertiseId},
    callAPI: {
      method: 'PUT',
      api:    `expertises/${expertiseId}`,
      body:   changeExpertise
    }
  }
}

export function editTechnology(technologyId, changeTechnology) {
  return {
    type:    EDIT_TECHNOLOGY_BY_ID,
    payload: {technologyId},
    callAPI: {
      method: 'PUT',
      api:    `technologies/${technologyId}`,
      body:   changeTechnology
    }
  }
}

export function addExpertise(expertise) {
  return {
    type:    ADD_EXPERTISE,
    callAPI: {
      method: 'PUT',
      api:    'expertises',
      body:   expertise
    }
  }

}

export function filterTechnologies(filters) {
  return {
    type:    FILTER_TECHNOLOGIES,
    callAPI: {
      method: 'POST',
      api:    'technologies',
      body:   filters

    }
  }
}

export function addTechnology(technology) {
  return {
    type:    ADD_TECHNOLOGY,
    callAPI: {
      method: 'PUT',
      api:    'technologies',
      body:   technology
    }
  }
}

export function addTag(tag) {
  return {
    type:    ADD_TAG,
    callAPI: {
      method: 'PUT',
      api:    'tags',
      body:   tag
    }
  }
}

export function deleteTag(tagId) {
  return {
    type:    DELETE_TAG_BY_ID,
    payload: {tagId},
    callAPI: {
      method: "DELETE",
      api:    `tags/${tagId}`
    }
  }
}

export function editTagById(tag) {
  return {
    type: EDIT_TAG_BY_ID,
    payload: {tagId: tag.id},
    callAPI: {
      method: 'PUT',
      api: `tags/${tag.id}`,
      body: tag
    }
  }
}
