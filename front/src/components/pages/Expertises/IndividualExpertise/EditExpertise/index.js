import React from 'react';
import Form from 'react-validation/build/form';
import {Modal, Button, FormGroup, ControlLabel, FormControl} from 'react-bootstrap';
import PopupWrapper from '../../../../../decorators/PopupWrapper/';
import {connect} from 'react-redux';
import {editExpertise} from '../../../../../AC/common';


class EditExpertise extends React.Component {

  state = {
    name:        '',
    description: '',
  };

  render() {
    const {changeState, expertise: {name, description}} = this.props;

    return (
      <div>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title">Edit Expertise</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormGroup controlId="formInlineName">
              <ControlLabel>Name</ControlLabel>
              <FormControl type="text" placeholder="Enter Name" value={this.state.name || name}
                           onChange={this.onChangeValue('name')}/>
            </FormGroup>

            <FormGroup controlId="formInlineName">
              <ControlLabel>Description</ControlLabel>
              <FormControl componentClass="textarea" placeholder="Enter description"
                           value={this.state.description || description || undefined}
                           onChange={this.onChangeValue('description')}/>
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={changeState}>Close</Button>
          <Button onClick={this.saveChanges}>Save</Button>
        </Modal.Footer>
      </div>
    );
  }

  onChangeValue = (field) => ev => {
    this.setState({
      [field]: ev.target.value
    });
  };

  saveChanges = () => {
    const {editExpertise, expertise, changeState} = this.props;
    const {name, description} = this.state;
    const changebleTechnology = {...expertise.toObject(), name, description};

    editExpertise(expertise.id, changebleTechnology);
    changeState();
  }

}

export default connect(null, {editExpertise})(PopupWrapper(EditExpertise));