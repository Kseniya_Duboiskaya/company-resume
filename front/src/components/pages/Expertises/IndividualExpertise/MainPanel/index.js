import React from 'react';
import EditExpertise from '../EditExpertise/'
import DeleteExpertise from '../DeleteExpertise/';

export default class MainPanel extends React.Component {

  state = {
    isOpenEditable:  false,
    isOpenDeletable: false
  };

  render() {
    const {expertise} = this.props;

    return (
      <div className="jumbotron indent-top">
        {this.state.isOpenEditable ?
          <EditExpertise isOpen={this.state.isOpenEditable} changeState={this.changeStateEditablePopup}
                         expertise={expertise}/> : null}


        {this.state.isOpenDeletable ?
          <DeleteExpertise isOpen={this.state.isOpenDeletable} changeState={this.changeStateDeletePopup}
                           expertise={expertise}/> : null}

        <h2 className='indent-bottom'>{expertise.name}</h2>
        <button className="button green medium indent-right-5" onClick={this.changeStateEditablePopup}>Edit
        </button>
        <button className="button red medium" onClick={this.changeStateDeletePopup}>Delete</button>
      </div>
    )
  }

  changeStateEditablePopup = () => {
    this.changeState('isOpenEditable');
  };

  changeStateDeletePopup = () => {
    this.changeState('isOpenDeletable');
  };

  changeState(field) {
    this.setState((prevState, props) => {
      return {
        [field]: !prevState[field]
      }
    })
  }

}

