import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import PopupWrapper from '../../../../../decorators/PopupWrapper/';
import {deleteExpertise} from '../../../../../AC/common';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';

class DeleteTechnology extends React.Component {

  render() {
    const {changeState, expertise: {name}} = this.props;

    return (
      <div>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title">Delete Expertise</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure delete expertise {name}?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={changeState}>Close</Button>
          <NavLink to='/expertises'><Button onClick={this.deleteExpertise}>Delete</Button></NavLink>
        </Modal.Footer>
      </div>
    );
  }

  deleteExpertise = () => {
    const {expertise, deleteExpertise} = this.props;
    deleteExpertise(expertise.id);
  }
}

export default connect(null, {deleteExpertise})(PopupWrapper(DeleteTechnology));