import React from 'react';
import {connect} from 'react-redux';
import {mapToArr} from '../../../../helpers/';
import {
  getExpertiseById,
  getAllTechnologies,
  deleteTechnologyFromExpertise,
  addTechnologyToExpertise
} from '../../../../AC/common';
import Select from '../../../Filters/Select';
import MainPanel from "./MainPanel/";

class IndividualExpertise extends React.Component {

  componentDidMount() {
    const {getExpertiseById, id} = this.props;
    getExpertiseById(id);
  };

  render() {
    const {expertise} = this.props;
    if (!expertise) return <h1>Loading...</h1>;

    return (
      <div>
        <div className="container">
          <MainPanel expertise={expertise}/>
          {this.getBody()}
        </div>
      </div>
    )
  }

  getBody() {
    const {technologies, expertise, getAllTechnologies} = this.props;
    if (!technologies.length) getAllTechnologies();

    return (<div className="container-for-selected">
      <div className='indent-bottom'>
        <h2>Technologies</h2>
        <Select selection={expertise.technologies} changeSelection={this.changeSelectionTechnologies}
                options={technologies}
                multi={true}/>
      </div>
    </div>);
  }


  changeSelectionTechnologies = (selection) => {
    const {expertise, id, deleteTechnologyFromExpertise, addTechnologyToExpertise} = this.props;
    if (expertise.technologies.length < selection.length) {
      const selectedItemId = selection[selection.length - 1].value;
      addTechnologyToExpertise(id, selectedItemId);
      return;
    }

    const removedItemId = this.findDeleted(selection, expertise.technologies);
    deleteTechnologyFromExpertise(id, removedItemId);

  };

  findDeleted(selection, technologies) {
    const selectionIds = selection.map((select) => {
      return select.value;
    });

    return technologies.filter((technology) => {
      return !selectionIds.includes(technology.id);
    })[0].id;
  }

}

export default connect((state, ownProps) => {
  return {
    expertise:    state.expertises.entities.get(ownProps.id),
    technologies: mapToArr(state.technologies.entities),
  }
}, {
  getExpertiseById,
  getAllTechnologies,
  deleteTechnologyFromExpertise,
  addTechnologyToExpertise
})(IndividualExpertise);