import React from 'react';
import {connect} from 'react-redux';
import {mapToArr} from '../../../helpers/';
import './style.scss';
import {NavLink} from 'react-router-dom';
import {deleteExpertise, editExpertise} from '../../../AC/common';
import InlineEdit from 'react-edit-inline';
import toggleOpen from '../../../decorators/toggleOpen';
import AddExpertise from './AddExpertise/';

class Expertises extends React.Component {
  render() {
    const {isOpen, toggleOpen} = this.props;
    
    return (
      <div>
        <AddExpertise changeState={toggleOpen} isOpen={isOpen}/>
        <div className="container">
          <div className="jumbotron indent-top">
            <h2>Expertises</h2>
            <button className="button green medium" onClick={this.addExpertise}>Add</button>
          </div>
          {this.getBody()}
        </div>
      </div>
    )
  }

  getBody() {
    const {expertises}         = this.props;
    const arrStringOfExpertise = expertises.map((expertise) => {
      return <tr key={expertise.id}>
        <td><NavLink to={`/expertises/${expertise.id}`}>{expertise.name}</NavLink></td>

        <td className={expertise.description ? '' : 'empty-cell'}><InlineEdit
          text={expertise.description || 'add description'}
          paramName="description"
          change={this.changeDescription(expertise)}
        /></td>

        <td>
          <button className="button red small" onClick={this.deleteExpertise(expertise.id)}>×</button>
        </td>
      </tr>
    });

    return (
      <table className="table__expertise table__striped">
        <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        {arrStringOfExpertise}
        </tbody>
      </table>
    )
  }

  addExpertise = () => {
    const {toggleOpen} = this.props;
    toggleOpen();
  };

  deleteExpertise = expertiseId => ev => {
    const {deleteExpertise} = this.props;
    deleteExpertise(expertiseId);
  };

  changeDescription = (expertise) => (value) => {
    const {editExpertise} = this.props;
    editExpertise(expertise.id, {...expertise.toObject(), description: value.description});
  }

}

export default connect((state) => {
  return {
    expertises: mapToArr(state.expertises.entities),
  }
}, {deleteExpertise, editExpertise})(toggleOpen(Expertises));