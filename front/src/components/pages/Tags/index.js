import React from 'react';
import {connect} from 'react-redux';
import {mapToArr} from '../../../helpers/';
import {
  getAllTags,
  deleteTag,
  editTagById
} from '../../../AC/common';
import toggleOpen from '../../../decorators/toggleOpen';
import AddTag from './AddTag/'
import InlineEdit from 'react-edit-inline';

class Tags extends React.Component {

  componentDidMount() {
    const {getAllTags} = this.props;
    getAllTags();
  };

  render() {
    const {isOpen, toggleOpen} = this.props;

    return (
      <div className="container">
        <AddTag changeState={toggleOpen} isOpen={isOpen}/>
        <div className="jumbotron indent-top">
          <h2>TAGS</h2>
          <button className="button green medium" onClick={this.addTag}>Add</button>
        </div>
        <div>{this.getAllTags()}</div>
      </div>
    )
  }

  addTag = () => {
    const {toggleOpen} = this.props;
    toggleOpen();
  };

  getAllTags() {
    const {tags}    = this.props;
    const arrOfTags = tags.map((tagItem) => {
      return <tr key={tagItem.tag.id}>
        <td><InlineEdit
          text={tagItem.tag.name}
          paramName="name"
          change={this.changeTag(tagItem.tag)}
        /></td>

        <td>
          <button className="button red small" onClick={this.deleteTag(tagItem.tag.id)}>×</button>
        </td>
      </tr>
    });

    return (
      <table className="table__expertise table__striped">
        <thead>
        <tr>
          <th>Name</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        {arrOfTags}
        </tbody>
      </table>
    )
  }

  deleteTag = (id) => () => {
    const {deleteTag} = this.props;
    deleteTag(id);
  };

  changeTag = (tag) => (value) => {
    const {editTagById} = this.props;
    const newTag = {...tag, ...value};

    editTagById(newTag);
  }

}

export default connect((state) => {
  return {
    tags: mapToArr(state.tags.entities)
  }
}, {getAllTags, deleteTag, editTagById})(toggleOpen(Tags));