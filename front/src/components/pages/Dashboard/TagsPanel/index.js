import React from 'react';
import Tag from './Tag/';

export default class TagsPanel extends React.Component {
  render() {
    const {tags}     = this.props;
    const numberLeft = Math.floor(tags.length / 2);

    const arrTags = tags.map((entity) => {
      return <Tag entity={entity} key={entity.tag.id}/>
    });

    const arrTagsLeft = arrTags.filter((tag, index) => {
      return index <= numberLeft;
    });

    const arrTagsRight = arrTags.filter((tag, index) => {
      return index > numberLeft;
    });

    return (
      <div className="accordeons">
        <div className="left">
          {arrTagsLeft}
        </div>
        <div className='right'>
          {arrTagsRight}
        </div>
      </div>
    )
  }
}