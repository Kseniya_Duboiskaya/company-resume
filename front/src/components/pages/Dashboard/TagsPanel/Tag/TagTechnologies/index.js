import React from 'react';
import {NavLink} from 'react-router-dom';

export default class TagTechnologies extends React.Component {

  render() {
    const {technologies}  = this.props;
    const arrTechnologies = technologies.map((technology) => {
      return <li key={technology.id}><NavLink to={`/technologies/${technology.id}`}>{technology.name}</NavLink></li>;
    });

    return (
      <ul>
        {arrTechnologies}
      </ul>
    )
  }
}