import React from 'react';
import './style.scss';
import TagTechnologies from './TagTechnologies/index';

export default class Tag extends React.Component {

  state = {
    isOpen: false
  };

  render() {
    const {entity: {tag}} = this.props;
    const technologies    = this.state.isOpen ? <TagTechnologies technologies={this.props.entity.technologies}/> : null;

    return (
      <div className="drop__button">
        <input type="checkbox" id={tag.id}/>
        <label htmlFor={tag.id} onClick={this.getTagTechnologies}>{tag.name}
        </label>
        {technologies}
      </div>
    )
  }

  getTagTechnologies = () => {
    this.setState((prevState, props) => ({
      isOpen: !prevState.isOpen
    }))

  };
}