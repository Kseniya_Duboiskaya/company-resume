import React from 'react';
import './style.scss';
import {getExpertises, getTagsByExpertise} from '../../../AC/common';
import {connect} from 'react-redux';
import {mapToArr} from '../../../helpers/';
import TagsPanel from './TagsPanel/';

class Dashboard extends React.Component {

  componentDidMount() {
    const {getExpertises} = this.props;
    getExpertises();
  };

  render() {
    const {expertises, tags} = this.props;
    const arrExpertises      = expertises.map((expertise) => <button className="button" key={expertise.id}
                                                                     onClick={this.getTagsFoeExpertise(expertise.id)}>{expertise.name}</button>);

    return (
      <div className="container">
        <div className="jumbotron indent-top">
          <h2>Expertises</h2>
          <div className="expertises">
            {arrExpertises}
          </div>
        </div>
        {tags.length ? this.getTags(tags) : null}
      </div>
    )
  }

  getTagsFoeExpertise = expertise => ev => {
    const {getTagsByExpertise} = this.props;
    getTagsByExpertise(expertise);
  };

  getTags = (tags) => {
      return <TagsPanel tags={tags}/>
  }

}

export default connect((state) => {
  return {
    expertises: mapToArr(state.expertises.entities),
    tags:       mapToArr(state.tags.entities),
  }
}, {getExpertises, getTagsByExpertise})(Dashboard);