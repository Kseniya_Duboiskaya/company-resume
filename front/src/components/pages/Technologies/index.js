import React from 'react';
import './style.scss';
import {mapToArr} from '../../../helpers/';
import {Modal, Button, FormGroup, ControlLabel, FormControl,} from 'react-bootstrap';
import Select from '../../Filters/Select';
import {connect} from 'react-redux';
import {
  getAllTechnologies,
  getAllTags,
  getExpertises,
  filterTechnologies,
  addTechnology
} from '../../../AC/common';
import PanelAllTechnologies from './PanelAllTechnologies/';

class Technologies extends React.Component {

  state = {
    name:        '',
    description: undefined,
    expertises:  [],
    tags:        []
  };

  componentDidMount() {
    const {getAllTechnologies, getExpertises, getAllTags} = this.props;
    getAllTechnologies();
    getExpertises();
    getAllTags();
  };

  getTagsOption(tags) {
    return tags.map((tagItem) => {
      return tagItem.tag;
    });
  };

  render() {
    const {expertises, tags, technologies} = this.props;
    const tagsOption                       = this.getTagsOption(tags);

    return (
      <div className="container">
        <div className="jumbotron indent-top">
          <h2>Tecnologies</h2>
        </div>

        <div className='indent-top'>
          <p className='font-24px'>New technology builder</p>

          <div className="creatable-panel col-md-12 ">
            <div className='creatable-item'>
              <ControlLabel className='col-md-2'><h3>Name</h3></ControlLabel>
              <FormControl type="text" placeholder="Enter Name" value={this.state.name}
                           onChange={this.onChangeValue('name')}/>
            </div>

            <div className='creatable-item'>
              <ControlLabel className='col-md-2'><h3>Description</h3></ControlLabel>
              <FormControl componentClass="textarea" placeholder="Enter Description" value={this.state.description}
                           onChange={this.onChangeValue('description')}/>
            </div>

            <div className='creatable-item'>
              <ControlLabel className='col-md-2'><h3>Expertises</h3></ControlLabel>
              <Select selection={this.state.expertises} changeSelection={this.changeSelectionExpertise}
                      options={expertises}
                      multi={true}/>
            </div>

            <div className='creatable-item tags'>
              <ControlLabel className='col-md-2'><h3>Tags</h3></ControlLabel>
              <Select selection={this.state.tags} changeSelection={this.changeSelectionTag}
                      options={tagsOption}
                      multi={true}
              />
            </div>

            <button className="button green medium" onClick={this.addTechnology}>Add</button>

          </div>
        </div>
        <PanelAllTechnologies technologies={technologies}/>
      </div>
    )
  }

  addTechnology = () => {
    const {addTechnology} = this.props;
    addTechnology(this.state);

  };

  onChangeValue = (value) => ev => {
    this.setState({
      [value]: ev.target.value
    });

  };

  changeSelectionExpertise = (selection) => {
    const {expertises} = this.props;
    this.changeSelectionItem(selection, expertises, 'expertises');
  };

  changeSelectionTag = (selection) => {
    const {tags}     = this.props;
    const tagsOption = this.getTagsOption(tags);
    this.changeSelectionItem(selection, tagsOption, 'tags');
  };

  changeSelectionItem(selection, items, field) {
    const {filterTechnologies} = this.props;
    const selectionIds         = selection.map((select) => {
      return select.value;
    });

    const itemsBySelectionId = items.filter((item) => {
      return selectionIds.includes(item.id);
    });

    this.setState({
      [field]: itemsBySelectionId
    }, () => {
      filterTechnologies({expertises: this.state.expertises, tags: this.state.tags})
    });

  };

}

export default connect((state) => {
  return {
    expertises:   mapToArr(state.expertises.entities),
    technologies: mapToArr(state.technologies.entities),
    tags:         mapToArr(state.tags.entities)
  }
}, {
  getAllTechnologies,
  getAllTags,
  getExpertises,
  filterTechnologies,
  addTechnology
})(Technologies);
