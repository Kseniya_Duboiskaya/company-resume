import React from 'react';
import {NavLink} from 'react-router-dom';

export default class PanelAllTechnologies extends React.Component {
  render() {
    return (
      <div>
        <p className='font-24px'>Existing Technologies</p>
        {this.getTechnologies()}
      </div>
    )
  }

  getTechnologies() {
    const {technologies}   = this.props;
    const technologiesItem = technologies.map((technology) => <button className="button"
                                                                      key={technology.id}><NavLink
      to={`/technologies/${technology.id}`}>{technology.name}</NavLink></button>);
    return (<div>{technologiesItem}</div>)
  }
}