import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import PopupWrapper from '../../../../../decorators/PopupWrapper/';
import {deleteTechnology} from '../../../../../AC/common';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';

class DeleteTechnology extends React.Component {

  render() {
    const {changeState, technology: {name}} = this.props;

    return (
      <div>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title">Delete Technology</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure delete technology {name}?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={changeState}>Close</Button>
          <NavLink to='/technologies'><Button onClick={this.deleteTechnology}>Delete</Button></NavLink>
        </Modal.Footer>
      </div>
    );
  }

  deleteTechnology = () => {
    const {technology, deleteTechnology} = this.props;
    deleteTechnology(technology.id);
  }
}

export default connect(null, {deleteTechnology})(PopupWrapper(DeleteTechnology));