import React from 'react';
import EditTechnology from '../EditTechnology/'
import DeleteTechnology from '../DeleteTechnology/';


export default class MainPanel extends React.Component {

  state = {
    isOpenEditable:  false,
    isOpenDeletable: false
  };

  render() {
    const {technology} = this.props;

    return (
      <div className="jumbotron indent-top">
        {this.state.isOpenEditable ?
          <EditTechnology isOpen={this.state.isOpenEditable} changeState={this.changeStateEditablePopup}
                          technology={technology}/> : null}


        {this.state.isOpenDeletable ?
          <DeleteTechnology isOpen={this.state.isOpenDeletable} changeState={this.changeStateDeletePopup}
                            technology={technology}/> : null}

        <h2 className='indent-bottom'>{technology.name}</h2>
        <button className="button green medium indent-right-5" onClick={this.changeStateEditablePopup}>Edit
        </button>
        <button className="button red medium" onClick={this.changeStateDeletePopup}>Delete</button>
      </div>
    )
  }

  changeStateEditablePopup = () => {
    this.changeState('isOpenEditable');
  };

  changeStateDeletePopup = () => {
    this.changeState('isOpenDeletable');
  };

  changeState(field) {
    this.setState((prevState, props) => {
      return {
        [field]: !prevState[field]
      }
    })
  }

}

