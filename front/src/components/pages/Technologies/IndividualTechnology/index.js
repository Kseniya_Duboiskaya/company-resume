import React from 'react';
import {connect} from 'react-redux';
import {getTechnologyById, updateTechnology} from '../../../../AC/common';
import './style.scss';
import Select from '../../../Filters/Select';
import {mapToArr} from '../../../../helpers/';
import MainPanel from './MainPanel/';

class IndividualTechnology extends React.Component {

  componentDidMount() {
    const {getTechnologyById, id} = this.props;
    getTechnologyById(id);
  };

  render() {
    const {technology} = this.props;
    if (!technology) return <h1>Loading...</h1>;

    return (
      <div>
        <div className="container">
          <MainPanel technology={technology}/>
          {this.getBody()}
        </div>
      </div>
    )
  }

  getBody() {
    const {technology, tags, expertises} = this.props;
    const arrTags                        = tags.map((tagItem) => tagItem.tag);

    return (<div className="container-for-selected">
      <div className='indent-bottom'>
        <h2>Expertises</h2>
        <Select selection={technology.expertises} changeSelection={this.changeSelectionExpertise}
                options={expertises}
                multi={true}/>
      </div>

      <div>
        <h2>Tags</h2>
        <Select selection={technology.tags} changeSelection={this.changeSelectionTags}
                options={arrTags}
                multi={true}/>
      </div>
    </div>);

  }

  changeSelectionExpertise = selection => {
    const {expertises} = this.props;
    this.changeSelectionItem(selection, expertises, 'expertises');
  };

  changeSelectionTags = selection => {
    const {tags}  = this.props;
    const arrTags = tags.map((tagItem) => tagItem.tag);

    this.changeSelectionItem(selection, arrTags, 'tags');
  };

  changeSelectionItem(selection, items, field) {
    const {id, technology, updateTechnology} = this.props;
    const selectionIds                       = selection.map((select) => {
      return select.value;
    });

    const itemsBySelectionId = items.filter((item) => {
      return selectionIds.includes(item.id);
    });

    const newTechnology = {...technology.toObject(), [field]: itemsBySelectionId};
    updateTechnology(newTechnology, id);
  };
}

export default connect((state, ownProps) => {
  return {
    expertises: mapToArr(state.expertises.entities),
    technology: state.technologies.entities.get(ownProps.id),
    tags:       mapToArr(state.tags.entities),
  }
}, {getTechnologyById, updateTechnology})(IndividualTechnology);