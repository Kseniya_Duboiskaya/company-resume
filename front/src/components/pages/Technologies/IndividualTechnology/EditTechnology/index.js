import React from 'react';
import Form from 'react-validation/build/form';
import {Modal, Button, FormGroup, ControlLabel, FormControl} from 'react-bootstrap';
import PopupWrapper from '../../../../../decorators/PopupWrapper/';
import {connect} from 'react-redux';
import {editTechnology} from '../../../../../AC/common';


class EditTechnology extends React.Component {

  state = {
    name:        '',
    description: '',
  };

  render() {
    const {changeState, technology: {name, description}} = this.props;

    return (
      <div>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title">Edit Technology</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormGroup controlId="formInlineName">
              <ControlLabel>Name</ControlLabel>
              <FormControl type="text" placeholder="Enter Name" value={this.state.name || name}
                           onChange={this.onChangeValue('name')}/>
            </FormGroup>

            <FormGroup controlId="formInlineName">
              <ControlLabel>Description</ControlLabel>
              <FormControl componentClass="textarea" placeholder="Enter description"
                           value={this.state.description || description || undefined}
                           onChange={this.onChangeValue('description')}/>
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={changeState}>Close</Button>
          <Button onClick={this.saveChanges}>Save</Button>
        </Modal.Footer>
      </div>
    );
  }

  onChangeValue = (field) => ev => {
    this.setState({
      [field]: ev.target.value
    });
  };

  saveChanges = () => {
    const {editTechnology, technology, changeState} = this.props;
    const {name, description} = this.state;
    const changebleTechnology = {...technology.toObject(), name, description};

    editTechnology(technology.id, changebleTechnology);
    changeState();
  }

}

export default connect(null, {editTechnology})(PopupWrapper(EditTechnology));