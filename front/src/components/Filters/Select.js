import React from 'react';
import ReactSelect from 'react-select';
import 'react-select/dist/react-select.css';

export default class Select extends React.Component {
  render() {
    const {options, selection, multi, changeSelection} = this.props;

    return (
      <div>
        <ReactSelect options={this.getOptions(options)} multi={multi} value={this.getOptions(selection)}
                     onChange={changeSelection}/>
      </div>
    )

  }

  getOptions(items) {
    return items.map((item) => {
      return {
        value: item.id,
        label: item.name,
      }
    });
  }
}

