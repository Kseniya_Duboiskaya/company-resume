import React from 'react';
import './style.scss';
import {Route, NavLink, Switch, Redirect} from 'react-router-dom';
import Dashboard from '../pages/Dashboard/';
import ExpertisePage from '../getRoutes/Expertises';
import Tags from '../pages/Tags/';
import TechnologyPage from '../getRoutes/Technologies'

export default class Navigation extends React.Component {
  render() {
    return (
      <div className="header">
        <ul className="navbar">
          <li className="logo"><NavLink to='/dashboard'>Zensoft</NavLink></li>
          <li><NavLink to='/expertises'>Expertises</NavLink></li>
          <li><NavLink to='/tags'>Tags</NavLink></li>
          <li><NavLink to='/technologies'>Technologies</NavLink></li>
        </ul>
        <Switch>
          <Route path='/' render={this.getRoute} exact/>
          <Route path='/dashboard' component={Dashboard}/>
          <Route path='/expertises' component={ExpertisePage}/>
          <Route path='/tags' component={Tags}/>
          <Route path='/technologies' component={TechnologyPage}/>
        </Switch>
      </div>
    )
  }

  getRoute = () => {
    return <Redirect to='/dashboard'/>
  }
};



