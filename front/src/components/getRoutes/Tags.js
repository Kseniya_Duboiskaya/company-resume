import React from 'react';
import {Route} from 'react-router-dom'
import IndividualExpertise from '../pages/Expertises/IndividualExpertise/';
import Expertises from '../pages/Expertises/';

function ExpertisePage({match}) {
  if (!match.isExact) return <Route path='/expertises/:id' render={getExpertisePage}/>; // если isExact то весь url совпал
  return <Route to='/expertises/' component={Expertises}/>
}

function getExpertisePage({match}) {
  return <IndividualExpertise id={+match.params.id}/>
}

export default ExpertisePage;
