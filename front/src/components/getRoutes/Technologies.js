import React from 'react';
import {Route} from 'react-router-dom'
import IndividualTechnology from '../pages/Technologies/IndividualTechnology/';
import Technologies from '../pages/Technologies/';


function TechnologyPage({match}) {
  if(!match.isExact) return <Route path='/technologies/:id' render = {getTechnologyPage}/>; // если isExact то весь url совпал
  return <Route to='/technologies/' component={Technologies}/>
}

function getTechnologyPage({match}) {
  return <IndividualTechnology id={+match.params.id}/>
}

export default TechnologyPage;
