import React from 'react';
import {Provider} from 'react-redux';
import store from '../../store/';
import {ConnectedRouter} from 'react-router-redux'
import history from '../../history';
import Navigation from '../Navigation/index';
import './style/style.scss';
// import {Redirect} from 'react-router-dom';

export default class AppContainer extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
            <Navigation/>
        </ConnectedRouter>
      </Provider>
    )
  }
}