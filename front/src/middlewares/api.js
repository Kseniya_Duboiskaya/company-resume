import {START, SUCCESS, FAIL} from '../const/common';
import {baseFetch} from './base-api';

export default store => next => action => {
  const {callAPI, type, ...rest} = action;
  if (!callAPI) return next(action);

  next({
    ...rest, type: type + START
  });

  return baseFetch(callAPI)
    .then((response) => {
      return next({...rest, type: type + SUCCESS, response})
    })
    .catch(error => next({...rest, type: type + FAIL, error}));


}