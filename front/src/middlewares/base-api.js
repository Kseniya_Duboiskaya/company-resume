export function baseFetch(callAPI) {

  return fetch(`/api/${callAPI.api}`,
    {
      method:  callAPI.method,
      headers: {
        "Content-type": "application/json",
        "Accept":       "application/json, text/plain, */*",
      },
      body:    JSON.stringify(callAPI.body)
    }
  )
    .then((res) => {
      if (callAPI.method === 'DELETE') {
        return null;
      }
      return res.json();
    })
}