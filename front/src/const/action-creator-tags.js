export const GET_ALL_TAGS          = 'GET_ALL_TAGS';
export const GET_TAGS_BY_EXPERTISE = 'GET_TAGS_BY_EXPERTISE';
export const ADD_TAG               = 'ADD_TAG';
export const DELETE_TAG_BY_ID      = 'DELETE_TAG_BY_ID';
export const EDIT_TAG_BY_ID        = 'EDIT_TAG_BY_ID';