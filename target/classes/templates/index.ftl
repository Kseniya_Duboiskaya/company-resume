<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="mainApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <#--<meta name="_csrf" content="${_csrf.token}"/>-->
    <#--<meta name="_csrf_header" content="${_csrf.headerName}"/>-->

    <title ng-bind="title + ' | Zensoft'">Zensoft</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="/webjars/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/webjars/angular-loading-bar/0.9.0/build/loading-bar.min.css"/>

    <!-- xeditable -->
    <link rel="stylesheet"  type="text/css" href="/webjars/angular-xeditable/0.1.11/dist/css/xeditable.css">

    <link rel="stylesheet" type="text/css" href="/webjars/spectrum/1.7.0/spectrum.css">

    <!-- notifications - all types -->
    <link rel="stylesheet" type="text/css" href="/resources/notifications/css/ns-default.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/notifications/css/ns-style-growl.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/notifications/css/ns-style-bar.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/notifications/css/ns-style-attached.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/notifications/css/ns-style-other.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/notifications/css/ns-style-theme.css"/>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="/css/main.css">

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#/dashboard">ZENSOFT</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar" bs-navbar>
            <ul class="nav navbar-nav">

                <li data-match-route="/dashboard"><a href="#/expertises"><i class="fa fa-cloud"></i> Expertises</a></li>
                <li data-match-route="/dashboard"><a href="#/tags"><i class="fa fa-tags"></i> Tags</a></li>
                <li data-match-route="/dashboard"><a href="#/technologies"><i class="fa fa-cogs"></i> Technologies</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <#--<li><a href="/logout"><i class="fa fa-sign-out"></i> Выйти</a></li>-->
            </ul>
        </div>
    </div>
</nav>


<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">

            <div class="slide-main-container">
                <div ng-view autoscroll="true" class="slide-main-animation"></div>
            </div>




        </div>
    </div>

</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/webjars/jquery/3.1.0/dist/jquery.min.js"></script>
<script src="/webjars/bootstrap/3.3.7/dist/js/bootstrap.min.js"></script>

<script src="/webjars/moment/2.14.1/min/moment.min.js"></script>
<script src="/webjars/moment-timezone/0.5.4/builds/moment-timezone.min.js"></script>

<script src="/webjars/angularjs/1.5.8/angular.min.js"></script>
<script src="/webjars/angular-sanitize/1.5.8/angular-sanitize.min.js"></script>
<script src="/webjars/angular-route/1.5.8/angular-route.min.js"></script>
<script src="/webjars/angular-animate/1.5.8/angular-animate.min.js"></script>

<script src="/webjars/angular-loading-bar/0.9.0/build/loading-bar.min.js"></script>

<script src="/webjars/lodash/4.13.1/dist/lodash.min.js"></script>

<!-- xeditable -->
<script src="/webjars/angular-xeditable/0.1.11/dist/js/xeditable.min.js"></script>

<script src="/webjars/spectrum/1.7.0/spectrum.js"></script>

<!-- modernirz -->
<script src="/resources/notifications/js/modernizr.js"></script>

<!-- Notifications -->
<script src="/resources/notifications/js/modernizr.custom.js"></script>
<script src="/resources/notifications/js/classie.js"></script>
<script src="/resources/notifications/js/notificationFx.js"></script>

<!-- this page specific scripts -->
<script src="/app/main/app.js"></script>
<script src="/app/services.js"></script>
<script src="/app/main/controllers/dashboard.js"></script>
<script src="/app/main/controllers/expertises.js"></script>
<script src="/app/main/controllers/expertise.js"></script>
<script src="/app/main/controllers/tags.js"></script>
<script src="/app/main/controllers/technologies.js"></script>
<script src="/app/main/controllers/technology.js"></script>

<script>

</script>
</body>
</html>
