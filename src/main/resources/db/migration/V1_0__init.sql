CREATE TABLE tags(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE technologies(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR NOT NULL UNIQUE,
  description VARCHAR
);

CREATE TABLE expertises(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR NOT NULL UNIQUE,
  description VARCHAR
);

CREATE TABLE tags_technologies(
  tag_id BIGINT REFERENCES tags,
  technology_id BIGINT REFERENCES technologies,
  PRIMARY KEY (tag_id, technology_id)
);

CREATE TABLE expertises_technologies(
  expertise_id BIGINT REFERENCES expertises,
  technology_id BIGINT REFERENCES technologies,
  PRIMARY KEY (expertise_id, technology_id)
);