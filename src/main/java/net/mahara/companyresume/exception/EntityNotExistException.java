package net.mahara.companyresume.exception;

/**
 * @author Kadach Alexey
 */
public class EntityNotExistException extends LogicException {

    public EntityNotExistException(String message) {
        super(message);
    }

}
