package net.mahara.companyresume.exception;

/**
 * @author Kadach Alexey
 */
public class LogicException extends RuntimeException {

    public LogicException(String message) {
        super(message);
    }

}
