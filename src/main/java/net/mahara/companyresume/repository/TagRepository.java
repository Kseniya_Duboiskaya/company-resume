package net.mahara.companyresume.repository;

import net.mahara.companyresume.model.entity.Tag;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Kadach Alexey
 */
@Repository
public interface TagRepository extends BaseRepository<Tag> {

    List<Tag> findAllByOrderByName();

}
