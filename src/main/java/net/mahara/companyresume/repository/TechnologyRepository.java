package net.mahara.companyresume.repository;

import net.mahara.companyresume.model.entity.Technology;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Kadach Alexey
 */
@Repository
public interface TechnologyRepository extends BaseRepository<Technology> {

    List<Technology> findAllByOrderByName();

}
