package net.mahara.companyresume.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author Kadach Alexey
 */
@NoRepositoryBean
public interface BaseRepository<T> extends JpaRepository<T, Long> {
}