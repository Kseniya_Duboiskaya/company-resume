package net.mahara.companyresume.repository;

import net.mahara.companyresume.model.entity.Expertise;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Kadach Alexey
 */
@Repository
public interface ExpertiseRepository extends BaseRepository<Expertise> {

    List<Expertise> findAllByOrderByName();

}
