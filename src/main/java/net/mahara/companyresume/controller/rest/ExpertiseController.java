package net.mahara.companyresume.controller.rest;

import net.mahara.companyresume.model.entity.Expertise;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.service.ExpertiseService;
import net.mahara.companyresume.service.TechnologyExpertiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Kadach Alexey
 */
@RestController
@RequestMapping("/api/expertises")
public class ExpertiseController {

    private final ExpertiseService expertiseService;
    private final TechnologyExpertiseService technologyExpertiseService;


    @Autowired
    public ExpertiseController(ExpertiseService expertiseService,
                               TechnologyExpertiseService technologyExpertiseService) {
        this.expertiseService = expertiseService;
        this.technologyExpertiseService = technologyExpertiseService;
    }

    @GetMapping
    public List<Expertise> getAll() {
        return expertiseService.getAll();
    }

    @GetMapping("/{id}")
    public Expertise get(@PathVariable Long id) {
        return expertiseService.get(id);
    }

    @PutMapping
    public Expertise save(@Valid @RequestBody Expertise expertise) {
        Expertise persistExpertise = expertiseService.save(expertise);
        List<Technology> technologies = expertise.getTechnologies();
        if (!CollectionUtils.isEmpty(technologies)) {
            technologies.forEach(technology ->
                    technologyExpertiseService.addExpertiseToTechnology(technology.getId(), persistExpertise.getId()));
        }

        return persistExpertise;
    }

    @PutMapping("/{id}")
    public Expertise update(@PathVariable Long id, @Valid @RequestBody Expertise expertise) {
        expertise.setId(id);
        return expertiseService.update(expertise);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        Expertise expertise = expertiseService.get(id);
        List<Technology> technologies = expertise.getTechnologies();
        if (!CollectionUtils.isEmpty(technologies)) {
            technologies.forEach(technology ->
                    technologyExpertiseService.removeExpertiseFromTechnology(technology.getId(), expertise.getId()));
        }
        expertiseService.delete(id);
    }

}
