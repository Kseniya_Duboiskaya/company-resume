package net.mahara.companyresume.controller.rest;

import net.mahara.companyresume.model.dto.FindTagsDto;
import net.mahara.companyresume.model.dto.TagTechnologiesDto;
import net.mahara.companyresume.model.entity.Tag;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.service.TagService;
import net.mahara.companyresume.service.TechnologyTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * @author Kadach Alexey
 */
@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;
    private final TechnologyTagService technologyTagService;


    @Autowired
    public TagController(TagService tagService, TechnologyTagService technologyTagService) {
        this.tagService = tagService;
        this.technologyTagService = technologyTagService;
    }

    @GetMapping
    public List<Tag> getAll() {
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public Tag get(@PathVariable Long id) {
        return tagService.get(id);
    }

    @PostMapping
    public Set<TagTechnologiesDto> find(@Valid @RequestBody FindTagsDto findTagsDto) {
        return tagService.getByExpertiseId(findTagsDto.getExpertiseId());
    }

    @PutMapping
    public Tag save(@Valid @RequestBody Tag tag) {
        return tagService.save(tag);
    }

    @PutMapping("/{id}")
    public Tag update(@PathVariable Long id, @Valid @RequestBody Tag tag) {
        tag.setId(id);
        return tagService.update(tag);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        Tag tag = tagService.get(id);
        List<Technology> technologies = tag.getTechnologies();
        if (!CollectionUtils.isEmpty(technologies)) {
            technologies.forEach(technology ->
                    technologyTagService.removeTagFromTechnology(technology.getId(), tag.getId()));
        }
        tagService.delete(id);
    }

}
