package net.mahara.companyresume.controller.rest;

import net.mahara.companyresume.model.dto.FindTechnologiesDto;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.service.TechnologyExpertiseService;
import net.mahara.companyresume.service.TechnologyService;
import net.mahara.companyresume.service.TechnologyTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Kadach Alexey
 */
@RestController
@RequestMapping("/api/technologies")
public class TechnologyController {

    private final TechnologyService technologyService;
    private final TechnologyExpertiseService technologyExpertiseService;
    private final TechnologyTagService technologyTagService;


    @Autowired
    public TechnologyController(TechnologyService technologyService,
                                TechnologyExpertiseService technologyExpertiseService,
                                TechnologyTagService technologyTagService) {
        this.technologyService = technologyService;
        this.technologyExpertiseService = technologyExpertiseService;
        this.technologyTagService = technologyTagService;
    }

    @GetMapping
    public List<Technology> getAll() {
        return technologyService.getAll();
    }

    @GetMapping("/{id}")
    public Technology get(@PathVariable Long id) {
        return technologyService.get(id);
    }

    @PostMapping
    public List<Technology> find(@Valid @RequestBody FindTechnologiesDto findTechnologiesDto) {
        return technologyService.getAllByTags(findTechnologiesDto.getTags(), findTechnologiesDto.getExpertises());
    }

    @PutMapping
    public Technology save(@Valid @RequestBody Technology technology) {
        return technologyService.save(technology);
    }

    @PutMapping("/{id}")
    public Technology update(@PathVariable Long id, @Valid @RequestBody Technology technology) {
        technology.setId(id);
        return technologyService.update(technology);
    }

    @PutMapping("/{technologyId}/expertises/{expertiseId}")
    public Technology addExpertise(@PathVariable Long technologyId, @PathVariable Long expertiseId) {
        return technologyExpertiseService.addExpertiseToTechnology(technologyId, expertiseId);
    }

    @PutMapping("/{technologyId}/tags/{tagId}")
    public Technology addTag(@PathVariable Long technologyId, @PathVariable Long tagId) {
        return technologyTagService.addTagToTechnology(technologyId, tagId);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        technologyService.delete(id);
    }

    @DeleteMapping("/{technologyId}/expertises/{expertiseId}")
    public Technology removeExpertise(@PathVariable Long technologyId, @PathVariable Long expertiseId) {
        return technologyExpertiseService.removeExpertiseFromTechnology(technologyId, expertiseId);
    }

    @DeleteMapping("/{technologyId}/tags/{tagId}")
    public Technology removeTag(@PathVariable Long technologyId, @PathVariable Long tagId) {
        return technologyTagService.removeTagFromTechnology(technologyId, tagId);
    }

}
