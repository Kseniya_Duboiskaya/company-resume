package net.mahara.companyresume.controller.common;

import net.mahara.companyresume.model.dto.ExceptionDto;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @author Kadach Alexey
 */
@ControllerAdvice
public class ExceptionDtoControllerAdvice implements ResponseBodyAdvice<ExceptionDto> {

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        Class<?> parameterType = methodParameter.getParameterType();
        return parameterType.equals(ExceptionDto.class);
    }

    @Override
    public ExceptionDto beforeBodyWrite(ExceptionDto exceptionDto, MethodParameter methodParameter, MediaType mediaType,
                                        Class<? extends HttpMessageConverter<?>> aClass,
                                        ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        serverHttpResponse.setStatusCode(exceptionDto.getHttpStatus());
        return exceptionDto;
    }

}
