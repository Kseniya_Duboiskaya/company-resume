package net.mahara.companyresume.controller.common;

import net.mahara.companyresume.exception.EntityNotExistException;
import net.mahara.companyresume.model.dto.ExceptionDto;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Kadach Alexey
 */
@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(EntityNotExistException.class)
    public ExceptionDto entityNotExistExceptionHandler(EntityNotExistException exception) {
        return new ExceptionDto(exception.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ExceptionDto constraintViolationExceptionHandler(ConstraintViolationException exception) {
        return new ExceptionDto(exception.getSQLException().getMessage(), HttpStatus.METHOD_NOT_ALLOWED);
    }

}
