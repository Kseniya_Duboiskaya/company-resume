package net.mahara.companyresume.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Evgeni Krylov
 */
@Controller
public class MainController {

    @RequestMapping({"/", "/index.html"})
    public String index() {
        return "index";
    }

}
