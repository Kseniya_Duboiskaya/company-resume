package net.mahara.companyresume.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author Kadach Alexey
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionDto {

    private String message;

    private HttpStatus httpStatus;

}
