package net.mahara.companyresume.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.mahara.companyresume.model.entity.Tag;
import net.mahara.companyresume.model.entity.Technology;

import java.util.List;

/**
 * @author Kadach Alexey
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagTechnologiesDto {

    private Tag tag;

    private List<Technology> technologies;

}
