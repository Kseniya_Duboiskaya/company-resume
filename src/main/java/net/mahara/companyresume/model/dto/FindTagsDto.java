package net.mahara.companyresume.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author Kadach Alexey
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindTagsDto {

    @NotNull
    private Long expertiseId;

}
