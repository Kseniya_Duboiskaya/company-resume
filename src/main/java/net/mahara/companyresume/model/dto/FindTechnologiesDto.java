package net.mahara.companyresume.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.mahara.companyresume.model.entity.Expertise;
import net.mahara.companyresume.model.entity.Tag;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Kadach Alexey
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindTechnologiesDto {

    @NotNull
    private List<Tag> tags;

    @NotNull
    private List<Expertise> expertises;

}
