package net.mahara.companyresume.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Kadach Alexey
 */
@Data
@EqualsAndHashCode(callSuper = true, exclude = "technologies")

@Entity
@Table(name = "tags")
public class Tag extends BaseModel {

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "tags")
    private List<Technology> technologies;

}
