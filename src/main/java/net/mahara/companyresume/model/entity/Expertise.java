package net.mahara.companyresume.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Kadach Alexey
 */
@Data
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "expertises")
public class Expertise extends BaseModel {

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @JsonIgnoreProperties("expertises")
    @ManyToMany(mappedBy = "expertises")
    private List<Technology> technologies;

}
