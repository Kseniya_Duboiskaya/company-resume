package net.mahara.companyresume.service.impl;

import net.mahara.companyresume.exception.EntityNotExistException;
import net.mahara.companyresume.model.entity.Expertise;
import net.mahara.companyresume.model.entity.Tag;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.repository.TechnologyRepository;
import net.mahara.companyresume.service.TechnologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * @author Kadach Alexey
 */
@Transactional(readOnly = true)
@Service
public class DefaultTechnologyService implements TechnologyService {

    private final TechnologyRepository technologyRepository;


    @Autowired
    public DefaultTechnologyService(TechnologyRepository technologyRepository) {
        this.technologyRepository = technologyRepository;
    }

    @Override
    public List<Technology> getAll() {
        return technologyRepository.findAllByOrderByName();
    }

    @Override
    public List<Technology> getAllByTags(List<Tag> tags, List<Expertise> expertises) {
        return technologyRepository.findAll().stream()
                .filter(technology ->
                        CollectionUtils.isEmpty(tags) || technology.getTags().stream()
                                .map(Tag::getId)
                                .collect(toSet())
                                .containsAll(tags.stream().map(Tag::getId).collect(toSet())))
                .filter(technology ->
                        CollectionUtils.isEmpty(expertises) || expertises.stream()
                                .allMatch(expertise -> expertise.getTechnologies().stream()
                                        .map(Technology::getId)
                                        .collect(toSet()).contains(technology.getId())))
                .collect(toList());
    }

    @Override
    public Technology get(Long id) {
        Technology technology = technologyRepository.findOne(id);

        if (null == technology) {
            throw new EntityNotExistException(String.format("Technology with id %d not exist", id));
        }

        return technology;
    }

    @Transactional
    @Override
    public Technology save(Technology technology) {
        return technologyRepository.save(technology);
    }

    @Transactional
    @Override
    public Technology update(Technology technology) {
        Technology persistTechnology = get(technology.getId());

        persistTechnology.setName(technology.getName());
        persistTechnology.setDescription(technology.getDescription());
        persistTechnology.setTags(technology.getTags());
        persistTechnology.setExpertises(technology.getExpertises());

        return technologyRepository.save(persistTechnology);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        technologyRepository.delete(get(id));
    }

}
