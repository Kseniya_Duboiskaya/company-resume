package net.mahara.companyresume.service.impl;

import net.mahara.companyresume.exception.EntityNotExistException;
import net.mahara.companyresume.model.entity.Expertise;
import net.mahara.companyresume.repository.ExpertiseRepository;
import net.mahara.companyresume.service.ExpertiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Kadach Alexey
 */
@Transactional(readOnly = true)
@Service
public class DefaultExpertiseService implements ExpertiseService {

    private final ExpertiseRepository expertiseRepository;


    @Autowired
    public DefaultExpertiseService(ExpertiseRepository expertiseRepository) {
        this.expertiseRepository = expertiseRepository;
    }

    @Override
    public List<Expertise> getAll() {
        return expertiseRepository.findAllByOrderByName();
    }

    @Override
    public Expertise get(Long id) {
        Expertise expertise = expertiseRepository.findOne(id);

        if (null == expertise) {
            throw new EntityNotExistException(String.format("Expertise with id %d not exist", id));
        }

        return expertise;
    }

    @Transactional
    @Override
    public Expertise save(Expertise expertise) {
        return expertiseRepository.save(expertise);
    }

    @Transactional
    @Override
    public Expertise update(Expertise expertise) {
        Expertise persistExpertise = get(expertise.getId());

        persistExpertise.setName(expertise.getName());
        persistExpertise.setDescription(expertise.getDescription());

        return expertiseRepository.save(persistExpertise);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        expertiseRepository.delete(get(id));
    }

}
