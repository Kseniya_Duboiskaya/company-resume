package net.mahara.companyresume.service.impl;

import net.mahara.companyresume.model.entity.Tag;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.service.TagService;
import net.mahara.companyresume.service.TechnologyService;
import net.mahara.companyresume.service.TechnologyTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Kadach Alexey
 */
@Transactional(readOnly = true)
@Service
public class DefaultTechnologyTagService implements TechnologyTagService {

    private final TechnologyService technologyService;
    private final TagService tagService;


    @Autowired
    public DefaultTechnologyTagService(TechnologyService technologyService, TagService tagService) {
        this.technologyService = technologyService;
        this.tagService = tagService;
    }

    @Override
    public Technology addTagToTechnology(Long technologyId, Long tagId) {
        Technology persistTechnology = technologyService.get(technologyId);
        Tag persistTag = tagService.get(tagId);

        persistTechnology.getTags().add(persistTag);

        return technologyService.update(persistTechnology);
    }

    @Override
    public Technology removeTagFromTechnology(Long technologyId, Long tagId) {
        Technology persistTechnology = technologyService.get(technologyId);
        Tag persistTag = tagService.get(tagId);

        persistTechnology.getTags().remove(persistTag);

        return technologyService.update(persistTechnology);
    }

}
