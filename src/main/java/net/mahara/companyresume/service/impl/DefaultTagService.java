package net.mahara.companyresume.service.impl;

import net.mahara.companyresume.exception.EntityNotExistException;
import net.mahara.companyresume.model.dto.TagTechnologiesDto;
import net.mahara.companyresume.model.entity.Tag;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.repository.TagRepository;
import net.mahara.companyresume.service.ExpertiseService;
import net.mahara.companyresume.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * @author Kadach Alexey
 */
@Transactional(readOnly = true)
@Service
public class DefaultTagService implements TagService {

    private final TagRepository tagRepository;
    private final ExpertiseService expertiseService;

    @Autowired
    public DefaultTagService(TagRepository tagRepository, ExpertiseService expertiseService) {
        this.tagRepository = tagRepository;
        this.expertiseService = expertiseService;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAllByOrderByName();
    }

    @Override
    public Set<TagTechnologiesDto> getByExpertiseId(Long id) {
        List<Technology> technologies = expertiseService.get(id).getTechnologies();
        return technologies.stream()
                .flatMap(technology -> technology.getTags().stream())
                .map(tag -> new TagTechnologiesDto(tag, tag.getTechnologies().stream()
                        .filter(technologies::contains)
                        .collect(toList())))
                .collect(toSet());
    }

    @Override
    public Tag get(Long id) {
        Tag tag = tagRepository.findOne(id);

        if (null == tag) {
            throw new EntityNotExistException(String.format("Tag with id %d not exist", id));
        }

        return tag;
    }

    @Transactional
    @Override
    public Tag save(Tag tag) {
        return tagRepository.save(tag);
    }

    @Transactional
    @Override
    public Tag update(Tag tag) {
        Tag persistTag = get(tag.getId());

        persistTag.setName(tag.getName());

        return tagRepository.save(persistTag);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        tagRepository.delete(get(id));
    }

}
