package net.mahara.companyresume.service.impl;

import net.mahara.companyresume.model.entity.Expertise;
import net.mahara.companyresume.model.entity.Technology;
import net.mahara.companyresume.service.ExpertiseService;
import net.mahara.companyresume.service.TechnologyExpertiseService;
import net.mahara.companyresume.service.TechnologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Kadach Alexey
 */
@Transactional(readOnly = true)
@Service
public class DefaultTechnologyExpertiseService implements TechnologyExpertiseService {

    private final TechnologyService technologyService;
    private final ExpertiseService expertiseService;


    @Autowired
    public DefaultTechnologyExpertiseService(TechnologyService technologyService, ExpertiseService expertiseService) {
        this.technologyService = technologyService;
        this.expertiseService = expertiseService;
    }

    @Transactional
    @Override
    public Technology addExpertiseToTechnology(Long technologyId, Long expertiseId) {
        Technology persistTechnology = technologyService.get(technologyId);
        Expertise persistExpertise = expertiseService.get(expertiseId);

        persistTechnology.getExpertises().add(persistExpertise);

        return technologyService.update(persistTechnology);
    }

    @Transactional
    @Override
    public Technology removeExpertiseFromTechnology(Long technologyId, Long expertiseId) {
        Technology persistTechnology = technologyService.get(technologyId);
        Expertise persistExpertise = expertiseService.get(expertiseId);

        persistTechnology.getExpertises().remove(persistExpertise);

        return technologyService.update(persistTechnology);
    }

}
