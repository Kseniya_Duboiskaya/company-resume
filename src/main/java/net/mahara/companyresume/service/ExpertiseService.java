package net.mahara.companyresume.service;

import net.mahara.companyresume.model.entity.Expertise;

import java.util.List;

/**
 * @author Kadach Alexey
 */
public interface ExpertiseService {

    List<Expertise> getAll();

    Expertise get(Long id);

    Expertise save(Expertise expertise);

    Expertise update(Expertise expertise);

    void delete(Long id);

}
