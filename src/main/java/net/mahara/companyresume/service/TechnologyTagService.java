package net.mahara.companyresume.service;

import net.mahara.companyresume.model.entity.Technology;

/**
 * @author Kadach Alexey
 */
public interface TechnologyTagService {

    Technology addTagToTechnology(Long technologyId, Long tagId);

    Technology removeTagFromTechnology(Long technologyId, Long tagId);
    
}
