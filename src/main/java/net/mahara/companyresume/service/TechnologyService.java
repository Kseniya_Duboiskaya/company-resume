package net.mahara.companyresume.service;

import net.mahara.companyresume.model.entity.Expertise;
import net.mahara.companyresume.model.entity.Tag;
import net.mahara.companyresume.model.entity.Technology;

import java.util.List;

/**
 * @author Kadach Alexey
 */
public interface TechnologyService {

    List<Technology> getAll();

    List<Technology> getAllByTags(List<Tag> tags, List<Expertise> expertises);

    Technology get(Long id);

    Technology save(Technology technology);

    Technology update(Technology technology);

    void delete(Long id);

}
