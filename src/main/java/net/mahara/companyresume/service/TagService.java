package net.mahara.companyresume.service;

import net.mahara.companyresume.model.dto.TagTechnologiesDto;
import net.mahara.companyresume.model.entity.Tag;

import java.util.List;
import java.util.Set;

/**
 * @author Kadach Alexey
 */
public interface TagService {

    List<Tag> getAll();

    Set<TagTechnologiesDto> getByExpertiseId(Long id);

    Tag get(Long id);

    Tag save(Tag tag);

    Tag update(Tag tag);

    void delete(Long id);

}
