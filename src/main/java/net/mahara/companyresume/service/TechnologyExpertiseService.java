package net.mahara.companyresume.service;

import net.mahara.companyresume.model.entity.Technology;

/**
 * @author Kadach Alexey
 */
public interface TechnologyExpertiseService {

    Technology addExpertiseToTechnology(Long technologyId, Long expertiseId);

    Technology removeExpertiseFromTechnology(Long technologyId, Long expertiseId);

}
